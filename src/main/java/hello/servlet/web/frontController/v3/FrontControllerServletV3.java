package hello.servlet.web.frontController.v3;

import hello.servlet.web.frontController.ModelView;
import hello.servlet.web.frontController.MyView;
import hello.servlet.web.frontController.v3.contoller.MemberFormControllerV3;
import hello.servlet.web.frontController.v3.contoller.MemberListControllerV3;
import hello.servlet.web.frontController.v3.contoller.MemberSaveControllerV3;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "frontControllerServletV3", urlPatterns = "/front-controller/v3/*")
public class FrontControllerServletV3 extends HttpServlet {

    // KEY uri, VALUE : controller
    private Map<String, ControllerV3> controllerMap = new HashMap<>();

    public FrontControllerServletV3() {
        controllerMap.put("/front-controller/v3/members/new-form", new MemberFormControllerV3());
        controllerMap.put("/front-controller/v3/members/save", new MemberSaveControllerV3());
        controllerMap.put("/front-controller/v3/members", new MemberListControllerV3());
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestURI = request.getRequestURI(); //uri 찾기.

        ControllerV3 controller = controllerMap.get(requestURI); // 해당 uri 에 맞는 controller 찾기.

        if (controller == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND); //페이지 없거나 못찾을때 404 error
            return;
        }

        //paramMap
        Map<String, String> paramMap = createParamMap(request); // 파라미터 반환 {age=111, username=asdfadsf}
        ModelView mv = controller.process(paramMap); // 컨트롤러의 process 메소드에 알맞게 로직이 실행된다.

        String viewName = mv.getViewName(); // 뷰 이름 반환.
        MyView view = viewResolver(viewName); // 뷰 이름 병합.
        view.render(mv.getModel(), request, response); // 뷰 랜더링.
    }

    private MyView viewResolver(String viewName) {
        return new MyView("/WEB-INF/views/" + viewName + ".jsp");
    }

    private Map<String, String> createParamMap(HttpServletRequest request) {
        Map<String, String> paramMap = new HashMap<>();
        // parameterName 반복해서 paramMap 에 반환
        request.getParameterNames().asIterator()
                .forEachRemaining(paramName -> paramMap.put(paramName, request.getParameter(paramName)));
        return paramMap;
    }
}
