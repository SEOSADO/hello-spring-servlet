package hello.servlet.web.frontController.v5;

import hello.servlet.web.frontController.ModelView;
import hello.servlet.web.frontController.MyView;
import hello.servlet.web.frontController.v3.ControllerV3;
import hello.servlet.web.frontController.v3.contoller.MemberFormControllerV3;
import hello.servlet.web.frontController.v3.contoller.MemberListControllerV3;
import hello.servlet.web.frontController.v3.contoller.MemberSaveControllerV3;
import hello.servlet.web.frontController.v4.controller.MemberFormControllerV4;
import hello.servlet.web.frontController.v4.controller.MemberListControllerV4;
import hello.servlet.web.frontController.v4.controller.MemberSaveControllerV4;
import hello.servlet.web.frontController.v5.adapter.ControllerV3HandlerAdapter;
import hello.servlet.web.frontController.v5.adapter.ControllerV4HandlerAdapter;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "frontControllerServletV5", urlPatterns = "/front-controller/v5/*")
public class FrontControllerServletV5 extends HttpServlet {

    // 기존 ControllerV4 명시 : private Map<String, ControllerV4> controllerMap = new HashMap<>();


    private final Map<String, Object> handlerMappingMap = new HashMap<>(); // 변경 Objects로 어떤것이든 들어올 수 있게 변경
    private final List<MyHandlerAdapter> handlerAdapters = new ArrayList<>();     // 여러개의 handler를 담는 List를 만든다. List에 담겨있는 handler를 찾아서 반환 목적.

    public FrontControllerServletV5() {
        initHandlerMappingMap();
        initHandlerAdapters();
    }

    private void initHandlerMappingMap() {
        handlerMappingMap.put("/front-controller/v5/v3/members/new-form", new MemberFormControllerV3());
        handlerMappingMap.put("/front-controller/v5/v3/members/save", new MemberSaveControllerV3());
        handlerMappingMap.put("/front-controller/v5/v3/members", new MemberListControllerV3());

        // V4 추가
        handlerMappingMap.put("/front-controller/v5/v4/members/new-form", new MemberFormControllerV4());
        handlerMappingMap.put("/front-controller/v5/v4/members/save", new MemberSaveControllerV4());
        handlerMappingMap.put("/front-controller/v5/v4/members", new MemberListControllerV4());
    }

    private void initHandlerAdapters() {
        handlerAdapters.add(new ControllerV3HandlerAdapter());
        handlerAdapters.add(new ControllerV4HandlerAdapter());
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // MemberFormControllerV3
        // MemberFormControllerV4
        Object handler = getHandler(request);
        if (handler == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND); //페이지 없거나 못찾을때 404 error
            return;
        }

        // ControllerV3HandlerAdapter
        // ControllerV4HandlerAdapter
        MyHandlerAdapter adaptor = getHandlerAdaptor(handler);

        // model 반환.
        ModelView mv = adaptor.handle(request, response, handler);

        String viewName = mv.getViewName(); // 뷰 이름 반환.
        MyView view = viewResolver(viewName); // 뷰 이름 병합.
        view.render(mv.getModel(), request, response); // 뷰 랜더링.
    }

    private Object getHandler(HttpServletRequest request) {
        String requestURI = request.getRequestURI(); //uri 찾기.
        System.out.println("FrontControllerServletV5.getHandler : " + requestURI);
        return handlerMappingMap.get(requestURI);// 해당 uri 에 맞는 handler 찾기.
    }

    private MyHandlerAdapter getHandlerAdaptor(Object handler) {
        for (MyHandlerAdapter adapter : handlerAdapters) {
            if(adapter.supports(handler)){ // 만약 어댑터가 지원하는가 handler를
                return adapter;
            }
        }
        throw new IllegalArgumentException("handler adapter를 찾을 수 없습니다. handler = " + handler);
    }

    private MyView viewResolver(String viewName) {
        return new MyView("/WEB-INF/views/" + viewName + ".jsp");
    }

}
