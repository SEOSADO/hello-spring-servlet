package hello.servlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan // 서블릿 자동 등록
@SpringBootApplication
public class ServletApplication {
	// 초기 프로젝트 생성 -> 설정 강의 메뉴얼 확인 필요.
	public static void main(String[] args) {
		SpringApplication.run(ServletApplication.class, args);
	}



}
