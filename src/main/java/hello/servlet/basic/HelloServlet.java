package hello.servlet.basic;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "helloServlet", urlPatterns = "/hello")
public class HelloServlet extends HttpServlet {

    // ctrl + O
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("HelloServlet.service");
        System.out.println("request : " + req);
        System.out.println("res : " + resp);

        // ?username=sado 쿼리 파라미터 라고 한다.
        String username = req.getParameter("username");
        System.out.println("HelloServlet.service : "+ username);

        resp.setContentType("text/plain"); // HTTP Content-Type
        resp.setCharacterEncoding("utf-8"); // HTTP Content-Type
        resp.getWriter().write("hello"+username); //이렇게하면 HTTP body에 메시지가 들어간다.

        //
    }
}
