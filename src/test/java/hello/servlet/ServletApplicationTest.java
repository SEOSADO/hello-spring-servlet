package hello.servlet;

import hello.servlet.domain.member.Member;
import hello.servlet.domain.member.MemberRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class ServletApplicationTest {

	MemberRepository memberRepository = MemberRepository.getInstance();

	@AfterEach
	void afterEach() {
		memberRepository.clearStore();
	}

	@Test
	void save() {
		// given 이런게 주어졌을때
		Member member = new Member("hello", 29);

		// when 이런게 실행됬을때
		Member savedMember = memberRepository.save(member);

		// then 결과는?
		Member findMember = memberRepository.findById(savedMember.getId());
		System.out.println("findMember = "+findMember.getId() + "  "+findMember.getUsername()+"  "+findMember.getAge());

		Assertions.assertThat(findMember).isEqualTo(savedMember);
	}

	@Test
	void findAll() {
		//given
		Member member1 = new Member("sado1", 20);
		Member member2 = new Member("ssd", 29);

		memberRepository.save(member1);
		memberRepository.save(member2);

		//when
		List<Member> result = memberRepository.findAll();

		//then
		Assertions.assertThat(result.size()).isEqualTo(2);
		Assertions.assertThat(result).contains(member1, member2);
	}
}
